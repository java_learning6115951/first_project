package newroom;

public class Perfect_number {
	public static void main(String[] args) {
		int n = 6;
		// call isPerfect function to 
		// check if the number is perfect or not.
		if(isPerfect(n))
			System.out.println(n + "is a perfect number ");
		else
			System.out.println(n + "is not perfect number");
		
	}
    // return true if n is perfect
	private  static boolean isPerfect(int n) 
	{
		// TODO Auto-generated method stub
		// 1 is not perfect number 
		if(n == 1)
		return false;
		// sum will store the sum of proper divisors
		// As 1 is a proper divisor for all numbers 
		// initialised sum with 1 
		int sum = 1;
		
		// looping through the numbers the number to check
		// if they are divisors or not
		for(int i=2; i<n; i++)
		{
			if(n % i == 0)
			{
				sum += i;
			}
		}
		// if sum of divisors is equal to 
		// n, then n is a perfect number
		if(sum == n)
			return true;
		
		return false;
	}

}
