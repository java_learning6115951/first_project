package classRoom;

public class Roman_letters {
	
		
		    public static int romanToInt(String s) {
		        int result = 0;

		        for (int i = 0; i < s.length(); i++) {
		            int currentVal = getValue(s.charAt(i));
		            
		            // If it's not the last numeral and the next numeral has greater value
		            if (i < s.length() - 1 && getValue(s.charAt(i + 1)) > currentVal) {
		                result -= currentVal;
		            } else {
		                result += currentVal;
		            }
		        }

		        return result;
		    }

		    private static int getValue(char romanChar) {
		        switch (romanChar) {
		            case 'I': return 1;
		            case 'V': return 5;
		            case 'X': return 10;
		            case 'L': return 50;
		            case 'C': return 100;
		            case 'D': return 500;
		            case 'M': return 1000;
		            default: return 0;
		        }
		    }

		    public static void main(String[] args) {
		        // Test the conversion
		       // Scanner sc=new Scanner(System.in);
		        String romanNumeral ="MV";
		        int result = romanToInt(romanNumeral);
		        System.out.println("The integer value of Roman numeral " + romanNumeral + " is: " + result);
		    }
		
	}

