package newroom;

public class Pattern {
	public static void main(String[] args) {
		
		Pattern p = new Pattern();
		int n = 6;
		int m = 10;
//		p.squreHollow(n);
//		p.trianglePattern(n);
//		p.numberIncreasingPyramid(n);
//		p.numberIncreasing_reversePyramid(n);
//		p.numberChangingPyramid(n);
//		p.zoroOneTriangle(n);
//		p.palindrome_Triangle(n);
//		p.rhombus_Pattern(n);
//		p.hollowTriangle(n);
//		p.hollow_Diamond(n);
//		p.hollow_Hourglass_Pattern(n);
//		p.pascals_Triangle(n);
//		p.kPattern(n);
		p.number(m);
		
	}
       
	private void number(int n) {
		    
		for (int i = 1; i <= n; i++)   
		{  
		for (int j = 1; j < i; j++)   
		{  
		System.out.print(" ");  
		}  
		for (int k = i; k <= n; k++)   
		{   
		System.out.print(k+" ");  
		}  
		System.out.println();  
		}  
		for (int i = n-1; i >= 1; i--)   
		{  
		for (int j = 1; j < i; j++)   
		{  
		System.out.print(" ");  
		}  
		for (int k = i; k <= n; k++)  
		{  
		System.out.print(k+" ");  
		}  
		System.out.println();  
		}  
		}  
		
	

	private void kPattern(int n) {
		int i, j;

        // outer loop to handle rows
        for (i = n; i >= 1; i--) {

            // inner loop to handle columns
            for (j = 1; j <= i; j++) {
                System.out.print("*");
            }

            // printing new line for each row
            System.out.println();
        }

        // outer loop to handle rows
        for (i = 2; i <= n; i++) {

            // inner loop to handle columns
            for (j = 1; j <= i; j++) {
                System.out.print("*");
            }

            // printing new line for each row
            System.out.println();
        }
		
	}

	private void pascals_Triangle(int n) {
		 for (int i = 1; i <= n; i++) {
	            for (int j = 0; j <= n - i; j++) {

	                // for left spacing
	                System.out.print(" ");
	            }

	            // used to represent x(i, k)
	            int x = 1;
	            for (int k = 1; k <= i; k++) {

	                // The first value in a line is always 1
	                System.out.print(x + " ");
	                x = x * (i - k) / k;
	            }
	            System.out.println();
	        }
		
	}

	private void hollow_Hourglass_Pattern(int n) {
		int i, j;
	      
        // Printing the upper part
        for (i = 1; i <= n; i++) {
            // inner loop to print spaces.
            for (j = 1; j < i; j++) {
                System.out.print(" ");
            }
            // inner loop to print value of j.
            for (j = i; j <= n; j++) {
                if(j==i||j==n||i==1)
                    System.out.print("* ");
                else
                    System.out.print("  ");
            }

            // printing new line for each row
            System.out.println();
        }

        // Printing the lower part
        for (i = n - 1; i >= 1; i--) {
            // inner loop to print spaces.
            for (j = 1; j < i; j++) {
                System.out.print(" ");
            }
            // inner loop to print value of j.
            for (j = i; j <= n; j++) {
                if(j==i||j==n||i==1)
                    System.out.print("* ");
                else
                    System.out.print("  ");
            }
            // printing new line for each row
            System.out.println();
        }
		
	}

	private void hollow_Diamond(int n) {
		 int i, j;
	        int num = 1;
	        // outer loop to handle upper part
	        for (i = 1; i <= n; i++) {
	            // inner loop to print spaces
	            for (j = 1; j <= n - i; j++) {
	                System.out.print(" ");
	            }
	            // inner loop to print stars
	            for (j = 1; j <= 2 * i - 1; j++) {
	                if (j == 1 || j == 2*i-1)
	                    System.out.print("*");
	                else
	                    System.out.print(" ");
	            }
	            System.out.println();
	        }

	        // outer loop to handle lower part
	        for (i = n-1; i >= 1; i--) {
	            // inner loop to print spaces
	            for (j = 1; j <= n - i; j++) {
	                System.out.print(" ");
	            }
	            // inner loop to print stars
	            for (j = 1; j <= 2 * i - 1; j++) {
	                if (j == 1 || j == 2*i-1)
	                    System.out.print("*");
	                else
	                    System.out.print(" ");
	            }
	            System.out.println();
	        }
	 
		
	}

	private void hollowTriangle(int n) {
		for(int i=1; i<=n; i++) // 1<=6
		{
			// space loop 
			for(int space=i; space<n; space++) // 1<
			{
				System.out.print(" ");
			}
			// star and space loop 
			for(int star=1; star<=(2*i-1); star++)
			{
				if(star==1 || i==6 || star==(2*i-1))
					System.out.print("*");
				else
					System.out.print(" ");
			}
			
			System.out.println();
		}
	
		
	}

	private void rhombus_Pattern(int n) {
		// TODO Auto-generated method stub
	
		for(int i=1;i<=n; i++)
		{
			
		 for(int row=n; row>=i; row--)
		 {
		     System.out.print(" ");
		 }
	
		 for(int row1=1; row1<=6; row1++)
		 {
			 System.out.print("*");
		 }
		 System.out.println();
		}		
	}

	private void palindrome_Triangle(int n) {
		// TODO Auto-generated method stub
		
		for(int i=1; i<=n; i++)
		{
		
		for(int row=i; row<n; row++)
	    {
	      System.out.print("  ");
	     }

	    for(int row1=i; row1>=1; row1--)
	    {
	      System.out.print(row1+" ");
	    }
	    for(int row3=2; row3<=i; row3++)
	    {
	    	System.out.print(row3+" ");
	    }
	    System.out.println();
	    
		}
		
	}

	private void zoroOneTriangle(int n) {
		for(int row=1; row<=n; row++)
		{
			for(int no=row; no>=1; no--)
			{
				if(no%2 == 0)
					System.out.print("0 ");
				else
					System.out.print("1 ");
			}
			System.out.println(); // next line
		}
	
		
	}

	private void numberChangingPyramid(int n) {
		
		int i, j;
        int num = 1;
        // outer loop to handle number of rows
        for (i = 1; i <= n; i++) {
            // inner loop to handle number of columns
            for (j = 1; j <= i; j++) {
                // printing value of num in each iteration.
                System.out.print(num + " ");
                // increasing the value of num.
                num++;
            }

            // printing new line for each row
            System.out.println();
        }
		
	}

	private void numberIncreasing_reversePyramid(int n) {
		 int i, j;
	        // outer loop to handle number of rows
	        for (i = n; i >= 1; i--) {
	            // inner loop to handle number of columns
	            for (j = 1; j <= i; j++) {
	                // printing column values upto the row
	                // value.
	                System.out.print(j + " ");
	            }

	            // print new line for each row
	            System.out.println();
	        }
		
	}

	

	private void numberIncreasingPyramid(int n) {
		// TODO Auto-generated method stub
		int i, j;
        // outer loop to handle number of rows
        for (i = 1; i <= n; i++) {
            // inner loop to handle number of columns
            for (j = 1; j <= i; j++) {
                // printing column values upto the row
                // value.
                System.out.print(j + " ");
            }

            // print new line for each row
            System.out.println();
        }
		
	}

	private void trianglePattern(int n) {
int val=1,row;
		
		
		while(val<=n)
		  {
		for(row=val; row<n; row++)
		{
			System.out.print(" ");
		}
		for(row=1; row<=val; row++)
		{
			System.out.print(val+" ");
		}
		val++;
		System.out.println();
		
		  }
	}

	private void squreHollow(int n) {
		// TODO Auto-generated method stub
		int i, j;
        // outer loop to handle number of rows
        for (i = 0; i < n; i++) {
            //  inner loop to handle number of columns
            for (j = 0; j < n; j++) {
                // star will print only when  it is in first
                // row or last row or first column or last
                // column
                if (i == 0 || j == 0 || i == n - 1
                    || j == n - 1) {
                    System.out.print("*");
                }
                // otherwise print space only.
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
            
        }

		
	}

}
