package Zoho_interview_pgm;

import java.util.Arrays;
import java.util.Scanner;

public class Zoho {

	public static void main(String[] args) {
		
		Zoho zoho = new Zoho();
		
//		zoho.StringExpension();
//		zoho.runLengthDecoding();
//		zoho.anagram();
//		zoho.StringExpensionNoImbuild();
//		 String str = "geeksforgeeks"; 
//	        int len = str.length(); 
//		zoho.xFormat(str,len);
//		ogrzoho.mergeArray();
		
//		String s = "i like this program very much ";
//
//        // Function call
//        char[] p = reverseWords(s.toCharArray());
//        System.out.print(p);
		
		
		
		
		
	   
		zoho.CheckArraysEqual();
		
		int arr[] = { 64, 34, 25, 12, 22, 11, 90 }; 
		zoho.bubbleSort(arr); 
        System.out.println("Sorted array"); 
        zoho.printArray(arr); 
        
        int matrix[][] = { { 39, 27, 11, 42 },
                { 10, 93, 91, 90 },
                { 54, 78, 56, 89 },
                { 24, 64, 20, 65 } };
// Sort this matrix by 3rd Column
int col = 3;
sortbyColumn(matrix, col - 1);

// Display the sorted Matrix
for (int i = 0; i < matrix.length; i++) {

 for (int j = 0; j < matrix[i].length; j++)
     System.out.print(matrix[i][j] + " ");
 System.out.println();
 
 
        
 
 
 
 
 
 
 
		
		
		
	}
public static void sortbyColumn(int arr[][], int col)
{
    // Using built-in sort function Arrays.sort with lambda expressions
  
  Arrays.sort(arr, (a, b) -> Integer.compare(a[col],b[col])); // increasing order
    
}
	}
	void bubbleSort(int arr[]) 
	    { 
	        int n = arr.length; 
	        for (int i = 0; i < n - 1; i++) 
	            for (int j = 0; j < n - i - 1; j++) 
	                if (arr[j] > arr[j + 1]) { 
	                    // swap temp and arr[i] 
	                    int temp = arr[j]; 
	                    arr[j] = arr[j + 1]; 
	                    arr[j + 1] = temp; 
	                } 
	    } 
	  
	    // Prints the array 
	    void printArray(int arr[]) 
	    { 
	        int n = arr.length; 
	        for (int i = 0; i < n; ++i) 
	            System.out.print(arr[i] + " "); 
	        System.out.println(); 
	    } 
	
	private void CheckArraysEqual() {

        // Initializing the first array 
        int a[] = { 30, 25, 40 }; 
  
        // Initializing the second array 
        int b[] = { 30, 25, 40 }; 
  
        // store the result 
        // Arrays.equals(a, b) function is used to check 
        // whether two arrays are equal or not 
        boolean result = Arrays.equals(a, b); 
  
        // condition to check whether the 
        // result is true or false 
        if (result == true) { 
            // Print the result 
            System.out.println("Two arrays are equal"); 
        } 
        else { 
            // Print the result 
            System.out.println("Two arrays are not equal"); 
        }
		
	}

	static void reverse(char str[], int start, int end)
    {
        // Temporary variable
        // to store character
        char temp;

        while (start <= end) {
            // Swapping the first
            // and last character
            temp = str[start];
            str[start] = str[end];
            str[end] = temp;
            start++;
            end--;
        }
    }
    // Function to reverse words
    static char[] reverseWords(char[] s)
    {
        // Reversing individual words as
        // explained in the first step

        int start = 0;
        for (int end = 0; end < s.length; end++) {
            // If we see a space, we
            // reverse the previous
            // word (word between
            // the indexes start and end-1
            // i.e., s[start..end-1]
            if (s[end] == ' ') {
                reverse(s, start, end);
                start = end + 1;
            }
        }

        // Reverse the last word
        reverse(s, start, s.length - 1);

        // Reverse the entire String
        reverse(s, 0, s.length - 1);
        return s;
    }


	private void mergeArray() {
		int arr1[] = {1, 3, 5, 7};
        int n1 = arr1.length;
 
        int arr2[] = {2, 4, 6, 8};
        int n2 = arr2.length;
 
        int arr3[] = new int[n1 + n2];
        mergeArrays(arr1, arr2, n1, n2, arr3);
 
        System.out.println("Array after merging");
        for (int i=0; i < n1+n2; i++)
            System.out.print(arr3[i] + " ");
             
    }
     
    public static void mergeArrays(int[] arr1, int[] arr2, int n1, int n2, int[] arr3){
        int i = 0;  
        int j = 0;  
        int k = 0;  
         
        // traverse the arr1 and insert its element in arr3
        while(i < n1){   
         arr3[k++] = arr1[i++];   
        }   
         
        // now traverse arr2 and insert in arr3
        while(j < n2){   
         arr3[k++] = arr2[j++];   
        }   
         
        // sort the whole array arr3
        Arrays.sort(arr3);   
		
	}


	private void xFormat(String str, int len) {
		 // i and j are the indexes 
        // of characters to be 
        // displayed in the ith 
        // iteration i = 0 initially 
        // and go upto length of string 
        // j = length of string initially 
        // in each iteration of i, 
        // we increment i and decrement j, 
        // we print character only 
        // of k==i or k==j 
        for (int i = 0; i < len; i++) { 
            int j = len - 1 - i; 
            for (int k = 0; k < len; k++) { 
                if (k == i || k == j) 
                    System.out.print(str.charAt(k)); 
                else
                    System.out.print(" "); 
            } 
            System.out.println(""); 
        } 
		
	}


	private void StringExpensionNoImbuild() {
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter your a String ");
		String s = obj.nextLine();
		
		char[] ch = new char[s.length()];
		for(int i=0; i<s.length(); i++)
		{
			ch[i] = s.charAt(i);
		}
		
		for(int i=0; i<ch.length;i++)
		{
			int num = 0;
			if(ch[i]>='0' && ch[i]<='9')
			{
				char temp = ch[i-1];
				for(int j=i; j<ch.length; j++)
				{
					if(ch[j]>='0' && ch[j]<='9')
					{
						num = (num*10) + ch[i]-48;
					}
					else 
					{
						break;
					}
					i++;
				}
				for(int k=0; k<num; k++)
				{
					System.out.print(temp);
				}
			}
			
		}
		
	}


	private void anagram() {
		String a = "act";
		String b = "cat";
		
		char[] a1 = a.toCharArray();
		char[] b1 = b.toCharArray();
		
		int count=0;
		if(a1.length == b1.length)
		{
			for(int j=0; j<b1.length; j++)
			{
				char ch = b1[j];
				for(int i=0; j<a1.length; i++)
				{
					if(ch == a1[i])
						
					{
						count++; 
						break;
					}
				}
			}
			if(count == b.length())
			{
				System.out.println("Anagram");
			}
			else
				System.out.println("Not Anagram");
		}
		
	}


	private void runLengthDecoding() {
		Scanner obj = new Scanner(System.in);
		System.out.println("Enter a String ");
		String s = obj.nextLine(); // a1b10 or b3c6d15
		
		char[] ch = new char[s.length()];
		for(int i=0; i<s.length(); i++)
		{
			ch[i] = s.charAt(i);
		}
		for(int i=0; i<ch.length;i++)
		{
			if(ch[i] >='0' && ch[i]<='9')
			{
				char temp = ch[i-1];
			}
		}
		
	}

	
	
	private void StringExpension() {
		// output = "aabbbc"
		// input = "a2b3c1
		
		String str = "a2b3c1";
		
		
		for(int i=0; i<str.length();i++)
		{
			if(Character.isAlphabetic(str.charAt(i)))
			{
				System.out.print(str.charAt(i));
			}
			else
			{
				int x = Character.getNumericValue(str.charAt(i));
				for(int j=1; j<x; j++)
				{
					System.out.print(str.charAt(i-1));
					
				}
			}
		}
		
	}
}






