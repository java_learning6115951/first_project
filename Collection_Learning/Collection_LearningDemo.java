package Collection_Learning;

import java.util.Arrays;
import java.util.Comparator;
/*
 * வணக்கம் தோழர்களே, இப்போது நாம் என்ன உரையாட போகிறோம் என்றால் ,  java.util.Arrays
 * என்ற ஒரு Class ஐ  பற்றி தான், நமக்கு ஏற்கனவே தெரியும் java.util அப்படி என்று ஒரு Package இருக்கிறது
 *  அந்த Package ல்  நமக்கு தேவையாண நிறைய utility Classes இருக்கிறது. இப்போது ஒரு கேள்வி வரும்  
 *  utility Class என்றால் என்ன?
 *   பொதுவாக நமக்கு பயன்படுகிற கிளாஸ்ஸஸ் ஐ யுடிலிட்டி கிளாஸ் என்று சொல்கிறோம் 
 *   அந்த மாதிரியான கிளாஸ்ஸஸ் எல்லாவற்றையும் சேர்த்து வைக்கிற Packageற்கு java.util என்று பெயர் 
 *   அந்த Package ல Arrays என்ற ஒரு கிளாஸ் இருக்கிறது. சரி ok ,Arrays கிளாஸ் எதற்கு பயன்படும் ? 
 *   Arrays கிளாஸ் ஐ வைத்து நம்மிடம் உள்ள primitive Arrayவாக இருந்தாலும் சரி,
 *   அல்லது  non-primitive arrayவாக இருந்தாலும், அதை நாம் sort பண்ண முடியும் , முக்கியமாக 
 *   நமக்கு தேவையான மாதிரி sort பண்ண முடியும். அதற்கு பயன்படுகிற methods ஐ தான், 
 *    நாம் இப்போது பயன்படுத்தி பார்க்க போகிறோம். 
 *  இதற்கு தேவையான Prerequisites
 *  Eclipse IDE
 *  Inheritance
 *  Interface
 *  Dynamic Binding
 *  Type Casting 
 *  Return data type 
 *  இதை பற்றி தெரியும் என்றால் உங்களுக்கு கொஞ்சம் சுலபமாக இருக்கும் 
 */
public class Collection_LearningDemo {
	public static void main(String[] args) {
		// இப்போ நாம் Array பற்றி பார்ப்பதால் , ஒரு Array create பண்ணுவோம்  
		// அதற்கு random ஆக  value assign பண்ணியாச்சு. 
		int [] arr = {5,3,10, 18,92 };
		System.out.println("sort பண்ணுவதற்கு முன்  ");
		//for each loop ஐ  வைத்து இந்த array வில் உள்ள values ஐ பிரிண்ட் பண்ணி பார்ப்போம் 
		for(int i : arr)
		{
			System.out.print(i+",");
		}
		//  இந்த 5  numberம் கன்சோலில் output ஆக வந்தது. 
		// output - 5,3,10,18,92, 
		
		// intha arrayvai sort panna ninaikkre athukku oru SORT endru oru 
		// method irukku athu Arrays - Class la irukku 
		// Arrays - java.util - pacakage la irukku 
		
		Arrays.sort(arr); // intha sort method oru static method
		System.out.println();
		System.out.println("after sorting");
		for (int j : arr)
		{
			System.out.print(j+" ");
		}
		System.out.println();
		System.out.println("Mela ulla method Ascending order la answer kidaichathu   ******");
		System.out.println("_____________________");
		
		// itha sort method vachi non primiite data type sort panna mudiyuma ?
		/*
		 * namma entha Class create pannnalum sari, allathu java vil ullla Class aagan irunthalum 
		 * sari - athu - non primitive thaan
		 * 
		 */
		
		
		String[] names = { "Arul", "Viyan", "Niraimathi ", " Kavin "};
		System.out.println("______before String sorting ");
		for( String s : names)
		{
			System.out.println(s);
		}
		Arrays.sort(names);
		System.out.println("_____After String sorting ");
		System.out.println();
		
	
		for( String s : names)
		{
			System.out.println(s);
		}
		
		Comparator comp = new ComparatorDemo();
		Arrays.sort(names,comp);
		
		System.out.println();
		System.out.println("****After Comparator method usage *******");
		for(String t : names)
		{
			System.out.println(t);
		}
		
	}

}
