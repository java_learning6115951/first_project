
package Zoho_interview_pgm;

import java.util.Arrays;


public class Array_Pgm {
	static final int  N = 2;
	
  public static void main(String[] args) {
//	  int[] arr = { 10, 324, 45, 90, 9808 };
//      int n = arr.length;
//      System.out.println(largest(arr, n));
//      
	Array_Pgm ap = new Array_Pgm();
	
//	 System.out.println("Largest in given array is " + ap.findLargest_IterativeWay()); 
//	int find_mAX =  ap.find_Largest_Java8Streams();
//	System.out.println(find_mAX);
//	ap.findLargest_sortmethod();
//	
//	//print 2D Array
//	int mat[][] = { { 1, 2, 3, 4 },
//            { 5, 6, 7, 8 },
//            { 9, 10, 11, 12 } };
//	ap.print2dArray(mat);
	
	// Determinant of matrix
	//==========================
	
	int mat [][] = { { 4, 3 }, { 2, 3 } };
	
	System.out.println("Determinant of the matrix is :"
			+ determinantofMatrix(mat, N));
	
	
	}
	
	
 // Recursive function for finding determinant of 
  // matrix. n is current dimension of m[] [] 
private static String determinantofMatrix(int[][] mat, int n) {
  // Initialize result 
	int D = 0;
	// Base case : if matrix 
	// contains single element 
	
	if(n == 1)
	{
	  return mat[0][0];
	  // To store cofactors
	  
	  int temp [][] = new int [N][N];
	  
	  // To store sign multipler
	  int sign = 1;
	  
	  // To store sign mulitiplier
	  
	  
	  int sign = 1;
	 
	  // Iterate for each element of first row 
	  
	  for(int f=0; f<n; f++)
	  {
		  // Getting Cofactor of mat[0][f]
		  
		  getCofactor (mat,temp, 0, f, n);
		  D+= sign * mat[0][f]
				  * determinantofMatrix(temp, n-1);
		  
		  // terms are to be added 
		  // with alternate sign
		  
		  sign = - sign;
	  }
}
	return null;



}

  private static void getCofactor(int[][] mat, int[][] temp, int i, int f, int n2) {
	// TODO Auto-generated method stub
	
}


private void print2dArray(int[][] mat) {
	
	  // Loop through all rows
      for (int i = 0; i < mat.length; i++)

          // Loop through all elements of current row
          for (int j = 0; j < mat[i].length; j++)
              System.out.print(mat[i][j] + " ");
	
}

private void findLargest_sortmethod() {
	  int arr[] = { 12, 45, 67, 89, 100, 23,
			  3456, 897, 452, 444, 899, 700 };
	  
	  // Sorting function using 
	  // Sort function 
	  
	  Arrays.sort(arr);
	  
	  System.out.println("Largest number from sort method: "+
	   arr[arr.length-1]);
	  
	  
	  }
	
	


private int find_Largest_Java8Streams() {
	  int arr[] = {10, 324, 45, 90, 9808};
	  // Java Stream and max to find the max element in array
	  
	  int max = Arrays.stream(arr).max().getAsInt();
	  
	return max;
}

private int  findLargest_IterativeWay() {
	  int arr[] = {10, 324, 45, 90, 9808}; 

	  int i;
	  int max = arr[0];
	  
	  for(i=1; i<arr.length;i++)
	  {
		  if(arr[i]>max)
			  max = arr[i];
	  }	
	  return max;
}
  

// find largest number in insertion order
  
private static int largest(int[] arr, int n) {
	for (int i = 1; i < n; ++i) {
        int key = arr[i]; 
        int j = i - 1;

        // Move elements of arr[0..i-1], that are
        // greater than key, to one position ahead
        // of their current position 
        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }

    return arr[n-1];
}

}


