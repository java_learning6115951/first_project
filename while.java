public class while_practice
{
    public static void main(String args[])
    {
        int no=1;

        while(no<=10)
        {
            no++;
            System.out.println(no);
            no = no+1;
        }
    }

}


package MJU;

public class While_Program {
	public static void main(String[] args) {
		While_Program ll = new While_Program();
		ll.broken_Number(1234);
		ll.CountOfDigits(1234);
		ll.sumOfDigits(12345);
	}

	private void sumOfDigits(int i) {
		int cake = 12345;
		int sum = 0;
		while(cake>0)
		{
			sum = sum+cake%10;
			cake = cake/10;
		}
		System.out.println("12345 sum is "+sum);
	}

	private void CountOfDigits(int i) {
		int cake = 1234, count = 0;
		while(cake >0)
		{
			System.out.println(cake%10);
			
			cake = cake/10;
			count++;
		}
		System.out.println("digit count is  "+count);
		System.out.println("---------------------");
		
	}
	
	
	

	private void broken_Number(int i) {
		int cake = 1234;
		while(cake >0) 
		{
			System.out.print(cake%10+" ");
			cake = cake/10;
		}
		System.out.println();
		System.out.println("-----------------------");
		
	}

}
