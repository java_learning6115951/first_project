package leetcode;

public class FindLeastNumberofUinicElement {
	
	    public int findLeastNumOfUniqueInts(int[] arr, int k) {
	        Map<Integer,Integer> mp = new HashMap<>();

	        for(int i :arr)
	         mp.put(i,mp.getOrDefault(i,0)+1);

	        int[] freq = new int[mp.size()];
	        int i =0;
	        Iterator it = mp.keySet().iterator();
	        while(it.hasNext())
	        {
	            int key = (int) it.next();
	            int value = mp.get(key);
	            freq[i] = value;
	            i++;
	        }
	        for(int j:freq)
	        System.out.print(j+" ");
	    
	        Arrays.sort(freq);
	        int ans =0;
	        for(int j : freq)
	        {
	            if(j<=k)
	            {
	                k = k - j;
	            }else
	            {
	                ans++;
	            }
	        }
	        return ans;
	    }
	}

