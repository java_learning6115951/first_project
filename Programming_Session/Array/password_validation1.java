package mj_practice;

import java.util.Scanner;

public class password_validation1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your password without any space");
		String input = sc.next();

		password_validation1 obj = new password_validation1();

		obj.validate_pwd(input);
	}

	private void validate_pwd(String input) {

	int small = 0, spl_ch = 0, number = 0;
	  if (input.length() >= 8 && input.length()<=15) {
			if (input.charAt(0) >= 'A' && input.charAt(0) <= 'Z') {
				
				for (int i = 1; i < input.length(); i++) {
					if (input.charAt(i) >= 'a' && input.charAt(i) <= 'z')
						small++;
					else if (input.charAt(i) >= '0' && input.charAt(i) <= '9')
						number++;
					else if (input.charAt(i)>= '!' && input.charAt(i)<='/' || input.charAt(i)>= ':' && input.charAt(i)<='@' || input.charAt(i)>= '[' && input.charAt(i)<='`' || input.charAt(i)>= '{' && input.charAt(i)<='~')
						spl_ch++;
//					else if (input.charAt(i) >= 'A'&& input.charAt(i) <= 'Z')
//					{
//						NomoreCaps_Exception II=new NomoreCaps_Exception();
//						throw II;
//					}

				}

				if (small == 0) {
					NoSmallLetterException nse = new NoSmallLetterException();
					throw nse;
				}
			    if (spl_ch == 0) {
					NoSPLcharException nsp=new NoSPLcharException();
					throw nsp;
				}
			    if (number == 0) {
					NoNumberException NNE =new NoNumberException();
					throw NNE;
				}
			} 
			
			else {
				NoCapsException ncp = new NoCapsException();
				throw ncp;
			}
			System.out.println("Your Password Created Successfully");

		} 
		
		else {
			PasswordLengthMissmatchException pl = new PasswordLengthMissmatchException();
			throw pl ;
		}

	}
}

