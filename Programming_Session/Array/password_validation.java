package mj_practice;

import java.util.Scanner;

public class password_validation {

		public static void main(String[] args) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter your password without any space");
			String input = sc.next();

			password_validation obj = new password_validation();

			obj.validate_pwd(input);

		}

		private void validate_pwd(String input) {

			int small = 0, spl_ch = 0, number = 0;
			if (input.length() >= 8) {
				if (input.charAt(0) >= 'A' && input.charAt(0) <= 'Z') {
					for (int i = 1; i < input.length(); i++) {
						if (input.charAt(i) >= 'a' && input.charAt(i) <= 'z')
							small++;
						else if (input.charAt(i) >= '0' && input.charAt(i) <= '9')
							number++;
						else
							spl_ch++;

					}

					if (small == 0) {
						NoSmallLetterException nse = new NoSmallLetterException();
						throw nse;
					}
					if (spl_ch == 0) {
						System.out.println("No spl_ch Exception");
					}
					if (number == 0) {
						System.out.println("No number Exception");
					}
				} else {
					NoCapsException ncp = new NoCapsException();
					throw ncp;
				}
			} else {
				PasswordLengthMissmatchException plm = new PasswordLengthMissmatchException();
				throw plm;
			}

		}
	
}
	
	



