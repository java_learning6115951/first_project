package mj_practice;

public class Pattern_pgm2 {
	public static void main(String[] args) {
		Pattern_pgm2 pgm = new Pattern_pgm2();
		pgm.pattern1();
		System.out.println("----------------------------");
		pgm.pattern2();
		System.out.println("----------------------------");
		pgm.pattern3();
		System.out.println("----------------------------");
		pgm.pattern4();
		System.out.println("----------------------------");
		pgm.pattern5();
		System.out.println("----------------------------");
		pgm.pattern6();
		System.out.println("----------------------------");
		pgm.pattern7();
		System.out.println("----------------------------");
		pgm.pattern8();
		System.out.println("----------------------------");
		pgm.pattern9();
		System.out.println("----------------------------");
		pgm.pattern10();
		System.out.println("----------------------------");
		pgm.pattern11();
		System.out.println("----------------------------");
		pgm.pattern12();
		System.out.println("----------------------------");
		pgm.pattern13();
		System.out.println("----------------------------");
		pgm.pattern14();
		System.out.println("----------------------------");
		pgm.pattern15();
		System.out.println("----------------------------");
		
	}

	private void pattern15() {
		
		
	}

	private void pattern14() {
		for(int row=1; row<=5; row++)
		 {
		   for(int col=1; col<=5; col++)
		 {
		   if((row%2==0)&&col%2==0 || row%2==0)
		 {
		   System.out.print("1");
		 }
		   else
		   {
			   System.out.print("0");
		   }
		      
		 }
		   System.out.println();
		 }
		 
		
	}

	private void pattern13() {
		for(int row=1; row<=5; row++)
		 {
		   for(int col=1; col<=5; col++)
		 {
		   if(row==col)
		 {
		   System.out.print("1");
		 }
		   else
		   {
			   System.out.print("0");
		   }
		      
		 }
		   System.out.println();
		 }
		 
	}

	private void pattern12() {
		
		for(int row=1; row<=5; row++)
		 {
		   for(int col=1; col<=5; col++)
		 {
		   if(col%2!=0||row%2!=0)
		 {
		   System.out.print("1");
		 }
		   else
		   {
			   System.out.print("0");
		   }
		      
		 }
		   System.out.println();
		 }
		 
	}

	private void pattern11() {
		 for(int row=1; row<=5; row++)
		 {
		   for(int col=1; col<=5; col++)
		 {
		   if(col%2==1)
		 {
		   System.out.print("1");
		 }
		   else
		   {
			   System.out.print("0");
		   }
		      
		 }
		   System.out.println();
		 }
		 
	}
		      
		
	

	private void pattern10() 
	{
		for(int row=1; row<=5; row++)
		{
			for(int no=1; no<=6-row; no++)
			{
				System.out.print("  ");
			}
			for(int star =1;star<=row; star++) {
				System.out.print("*"+"   ");
			}
			System.out.println();
		}
	}

	private void pattern9() {

		for(int row=1; row<=5;row++)
		{
//			for(int col=1; col<row; col++)
//			{
//				System.out.print(" ");
//			}
			for(int star = 1; star<=row; star++)
			{
				System.out.print("*  ");
			}
			System.out.println();
		}		
		
		
		
	}

	private void pattern8() 
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<=row; col++)
			{
				System.out.print(" "+" ");
			}
			for(int star = 1; star<=6-row; star++)
			{
				System.out.print("*"+"  ");
			}
			System.out.println();
		}		
		
		
	}

	private void pattern7() 
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(" "+" ");
			}
			for(int star = 1; star<=3; star++)
			{
				System.out.print("*"+" ");
			}
			System.out.println();
		}		
		
		
		
	}

	private void pattern6() 
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(col+" ");
			}
			for(int star = 1; star<=row; star++)
			{
				System.out.print("*"+" ");
			}
			System.out.println();
		}		
		
	}

	private void pattern5()
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(col+" ");
			}
			for(int star = 1; star<6-row; star++)
			{
				System.out.print("*"+" ");
			}
			System.out.println();
		}
		
	}

	private void pattern4() 
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(col+" ");
			}
			for(int star = 1; star<=3; star++)
			{
				System.out.print("*");
			}
			System.out.println();
		}
		
		
	}

	private void pattern3() {

		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(col+" ");
			}
			for(int star = 1; star<=3; star++)
			{
				System.out.print("*");
			}
			System.out.println();
		}
		
		
	}
		

	private void pattern2() {
		
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(col+" ");
			}
			System.out.println();
		}
		
		
	}

	private void pattern1()
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<=6-row; col++)
			{
				System.out.print(col+" ");
			}
			System.out.println();
		}
		
		
	}

}
