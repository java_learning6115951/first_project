package mj_practice;

/*
    * * * * * * * 
    * * * * * * * 
    * * * * * * * 
    * * * * * * * 
    * * * * * * * 
    * * * * * * * 
    * * * * * * * 
    
 */

public class Pattern_Alphapets {
	
	public static void main(String[] args) {
		
		Pattern_Alphapets pa = new Pattern_Alphapets();
//		pa.pattern1();
	    
//		pa.pattern2();
		pa.pattern3();
	}
		
		

	private void pattern3() {
		for(int row=1; row<=7; row++)
		{
			for(int col=1; col<=7; col++)
			{
				if(row==1 || row==7)
					System.out.print("* ");
				
				else if (row ==col ||  row==8-col)
					System.out.print("* ");
				
				else
					System.out.print("  ");
				
			}
			System.out.println();
		}
		
	}



	private void pattern2() {
		
		              //  row + col =  8
		             //         row = 8 - col
		
//		* * * * * * *      1 + 7   = 8
//		  *       *        2 + 6
//		    *   *          3 + 5     
//		      *            4+  4     
//		    *   *          5 + 3  
//		  *       *        6 + 2
//		* * * * * * *      7 + 1
		
		for(int row=1; row<=7; row++)
		{
			for(int col=1; col<=7; col++)
			{
				if(row==1 || row==7) 
				
					System.out.print("* ");
				else if(row==col || row==8-col)
					System.out.print("* ");
		
				else
					System.out.print("  ");
				
			}
			System.out.println();
			
		}
		
	}
		



	private void pattern1() {
		for(int row=1; row<=7; row++)
		{
			for(int col=1; col<=7; col++)
			{
				System.out.print("* ");
			}
			System.out.println();
		}
		
	}
		
		
	}
