package newroom;
import java.util.Scanner;

public class PrimeNumber {
	public static void main(String[] args) {
		PrimeNumber pm = new PrimeNumber();
		pm.isPrime(17);
		if (isPrime(11))
			System.out.println(" true");
		else
			System.out.println(" false");
		if (isPrime(15))
			System.out.println(" true");
		else
			System.out.println(" false");
	}
	// JAVA program to demonstrate
	// Improved method
	// to check if a number is prime


	// Driver Class
	
		static boolean isPrime(int n)
		{
			// Corner case
			if (n <= 1)
				return false;

			// Check from 2 to n/2
			for (int i = 2; i <= n / 2; i++)
				if (n % i == 0)
					return false;

			return true;
		}

		// Driver Program
		
			
		
	}
