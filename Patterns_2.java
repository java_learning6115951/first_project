package mj_practice;

public class Pattern_pgm2 {
	public static void main(String[] args) {
		Pattern_pgm2 pgm = new Pattern_pgm2();
		pgm.pattern1();
		System.out.println("----------------------------");
		pgm.pattern2();
		System.out.println("----------------------------");
		pgm.pattern3();
		System.out.println("----------------------------");
		pgm.pattern4();
		pgm.pattern5();
		pgm.pattern6();
		pgm.pattern7();
		pgm.pattern8();
		pgm.pattern9();
		pgm.pattern10();
		
		
	}

	private void pattern10() 
	{
		for(int row=1; row<=5; row++)
		{
			for(int no=1; no<=6-row; no++)
			{
				System.out.print("  ");
			}
			for(int star =1;star<=row; star++) {
				System.out.print("*"+"   ");
			}
			System.out.println();
		}
	}

	private void pattern9() {

		for(int row=1; row<=5;row++)
		{
//			for(int col=1; col<row; col++)
//			{
//				System.out.print(" ");
//			}
			for(int star = 1; star<=row; star++)
			{
				System.out.print("*  ");
			}
			System.out.println();
		}		
		
		
		
	}

	private void pattern8() 
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<=row; col++)
			{
				System.out.print(" "+" ");
			}
			for(int star = 1; star<=6-row; star++)
			{
				System.out.print("*"+"  ");
			}
			System.out.println();
		}		
		
		
	}

	private void pattern7() 
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(" "+" ");
			}
			for(int star = 1; star<=3; star++)
			{
				System.out.print("*"+" ");
			}
			System.out.println();
		}		
		
		
		
	}

	private void pattern6() 
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(col+" ");
			}
			for(int star = 1; star<=row; star++)
			{
				System.out.print("*"+" ");
			}
			System.out.println();
		}		
		
	}

	private void pattern5()
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(col+" ");
			}
			for(int star = 1; star<6-row; star++)
			{
				System.out.print("*"+" ");
			}
			System.out.println();
		}
		
	}

	private void pattern4() 
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(col+" ");
			}
			for(int star = 1; star<=3; star++)
			{
				System.out.print("*");
			}
			System.out.println();
		}
		
		
	}

	private void pattern3() {

		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(col+" ");
			}
			for(int star = 1; star<=3; star++)
			{
				System.out.print("*");
			}
			System.out.println();
		}
		
		
	}
		

	private void pattern2() {
		
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<6-row; col++)
			{
				System.out.print(col+" ");
			}
			System.out.println();
		}
		
		
	}

	private void pattern1()
	{
		for(int row=1; row<=5;row++)
		{
			for(int col=1; col<=6-row; col++)
			{
				System.out.print(col+" ");
			}
			System.out.println();
		}
		
		
	}

}
